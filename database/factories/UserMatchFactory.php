<?php

use Faker\Generator as Faker;

$factory->define(ZoeTest\UserMatch::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'agent_id' => 2,
        'distance' => 500
    ];
});
