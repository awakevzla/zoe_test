<?php

use Faker\Generator as Faker;

$factory->define(ZoeTest\Profession::class, function (Faker $faker) {
    return [
        'name' => $faker->title
    ];
});
