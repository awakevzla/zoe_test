<?php

use Faker\Generator as Faker;

$factory->define(ZoeTest\ZipDistance::class, function (Faker $faker) {
    return [
        'origin'=> '50301',
        'destination' => '07039',
        'distance' => 5000.00
    ];
});
