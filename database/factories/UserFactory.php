<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(ZoeTest\User::class, function (Faker $faker) {
    $zipCodes = [
        "35801","99501","85001","72201","94203","90001","90209","80201","06101","19901","20001","32501","33124","32801",
        "30301","96801","83254","60601","62701","46201","52801","50301","67201","41701","70112","04032","21201","02101",
        "49036","49734", "55801","39530","63101","59044","68901","89501","03217","07039","87500","10001","27565","58282",
        "44101","74101","97201", "15201","02840","29020","57401","37201","78701","84321","05751","24517","98004","25813",
        "53201","82941"
    ];
    $gender = ['F','M'][rand(0,1)];
    return [
        'first_name' => ($gender == 'F')?$faker->firstNameFemale:$faker->firstNameMale,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'zip_code' => $zipCodes[rand(0, count($zipCodes)-1)],
        'age' => rand(18, 60),
        'gender' => $gender,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => Str::random(10)
    ];
});
