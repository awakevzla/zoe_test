<?php

use Illuminate\Database\Seeder;

class ProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professions = [
            ['id'=>1, 'name' => 'Corporate Finance'],
            ['id'=>2, 'name' => 'Commercial Banking'],
            ['id'=>3, 'name' => 'Investment Banking'],
            ['id'=>4, 'name' => 'Hedge Funds'],
            ['id'=>5, 'name' => 'Financial Planing']
        ];

        foreach ($professions as $profession){
            \ZoeTest\Profession::create($profession);
        }
    }
}
