<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $zipCodes = [
            "35801","99501","85001","72201","94203","90001","90209","80201","06101","19901","20001","32501","33124","32801",
            "30301","96801","83254","60601","62701","46201","52801","50301","67201","41701","70112","04032","21201","02101",
            "49036","49734", "55801","39530","63101","59044","68901","89501","03217","07039","10001","27565","58282","82941",
            "44101","74101","97201", "15201","02840","29020","57401","37201","78701","84321","05751","24517","98004","25813",
            "53201"
        ];

        \ZoeTest\User::create([
            'id' => 1,
            'first_name'=>'Zoe',
            'last_name'=>'Testing',
            'age'=>rand(18,60),
            'gender'=>['F','M'][rand(0,1)],
            'zip_code'=>$zipCodes[rand(0, count($zipCodes)-1)],
            'email'=>'testing@zoe.io',
            'password'=>bcrypt('123123'),
            'profession_id'=>rand(1,5)
        ]);

        for($i = 2; $i<= 10; $i++){
            $gender = ['F','M'][rand(0,1)];
            \ZoeTest\User::create([
                'id' => $i,
                'first_name'=>($gender == 'F')?$faker->firstNameFemale:$faker->firstNameMale,
                'last_name'=>$faker->lastName,
                'age'=>rand(18,60),
                'gender'=>$gender,
                'zip_code'=>$zipCodes[rand(0, count($zipCodes)-1)],
                'email'=>$faker->freeEmail,
                'password'=>bcrypt('123123'),
                'profession_id'=>rand(1,5)
            ]);
        }
        for($i = 11; $i<=100; $i++){
            $gender = ['F','M'][rand(0,1)];
            \ZoeTest\User::create([
                'id' => $i,
                'first_name'=>($gender == 'F')?$faker->firstNameFemale:$faker->firstNameMale,
                'last_name'=>$faker->lastName,
                'age'=>rand(18,60),
                'gender'=>$gender,
                'zip_code'=>$zipCodes[rand(0, count($zipCodes)-1)],
                'email'=>$faker->email,
                'password'=>bcrypt('123123')
            ]);
        }
    }
}
