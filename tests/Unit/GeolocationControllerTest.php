<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ZoeTest\ZipDistance;

class GeolocationControllerTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;
    /**
     * Test to calculate distance between two zipCodes
     * @test
     *
     * @return void
     */
    public function i_can_calculate_distance_with_maps()
    {
        $controller = $this->app->make("ZoeTest\Http\Controllers\GeolocationController");
        $response = $this->app->call([$controller, "getDistance"], ["50301", "07039"]);
        Log::info("Response from getDistance: ".$response);
        $this->assertNotNull($response);
        $this->assertIsInt($response);
        $expected = 1751930;
        $this->assertEquals($expected, $response);
    }

    /**
     * Test to verify if i could retrieve a distance already store on database to improve calculation
     * @test
     *
     * @return void
     */
    public function i_can_return_distance_from_database_test()
    {
        factory(ZipDistance::class)->create();
        $controller = $this->app->make("ZoeTest\Http\Controllers\GeolocationController");
        $response = $this->app->call([$controller, "getDistance"], ["50301", "07039"]);
        Log::info("Response from getDistance: ".$response);
        $this->assertNotNull($response);
        $this->assertEquals(5000, $response);
    }

    /**
     * Test to prove that i can't calculate distance between one/two empties zip Codes
     * @test
     *
     * @return void
     */

    public function i_can_not_calculate_distance_between_empty_zip_code_test()
    {
        $controller = $this->app->make("ZoeTest\Http\Controllers\GeolocationController");
        $response = $this->app->call([$controller, "getDistance"], ["", ""]);
        Log::info("Response from getDistance: ".$response);
        $this->assertFalse($response);

        $response = $this->app->call([$controller, "getDistance"], ["50301", ""]);
        Log::info("Response from getDistance: ".$response);
        $this->assertFalse($response);

        $response = $this->app->call([$controller, "getDistance"], ["", "50301"]);
        Log::info("Response from getDistance: ".$response);
        $this->assertFalse($response);
    }
}
