<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ZoeTest\Profession;
use ZoeTest\User;
use ZoeTest\UserMatch;

class WebInteractionTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testWelcome()
    {

        $this->get("/")
            ->assertSee(" ZOE Test");
    }

    public function testUserMatches()
    {
        $profession = factory(Profession::class)->create();
        $agent = factory(User::class)->create(['profession_id'=>$profession->id]);
        $contact = factory(User::class)->create(['first_name'=>'Testing','last_name' => 'Case']);
        factory(UserMatch::class)->create(['user_id'=>$contact->id,'agent_id'=>$agent->id]);
        $this->actingAs($agent);
        $this->assertAuthenticated();

        $this->get('/home')
            ->assertSee("Your Contacts")
            ->assertSee("Testing Case");
    }

    public function testAgentContact()
    {
        $profession = factory(Profession::class)->create();
        $agent = factory(User::class)->create(['profession_id'=>$profession->id,'first_name'=>'Testing','last_name' => 'Case']);
        $contact = factory(User::class)->create();
        factory(UserMatch::class)->create(['user_id'=>$contact->id,'agent_id'=>$agent->id]);
        $this->actingAs($contact);
        $this->assertAuthenticated();

        $this->get('/home')
            ->assertSee("Your Agent")
            ->assertSee("Testing Case")
            ->assertSee("Feel free to contact him!");
    }
}
