@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">User Info</div>

                    <div class="card-body">
                        <table class="table-active" border="1" style="border-collapse: collapse;" align="center" cellpadding="10">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Age</th>
                                <th>Profession</th>
                                <th>Email</th>
                                <th>ZipCode</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{$user->first_name}} {{$user->last_name}}</td>
                                <td>{{($user->gender == 'M')?'Male':'Female'}}</td>
                                <td>{{$user->age}}</td>
                                <td>{{($user->profession)?$user->profession->name:'N/A'}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->zip_code}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
