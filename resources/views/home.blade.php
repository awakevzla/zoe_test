@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Matches</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(\Illuminate\Support\Facades\Auth::user()->profession_id)
                        <table class="table-primary table-bordered table-hover" align="center" width="100%" cellpadding="10">
                            <thead>
                            <tr>
                                <th colspan="5" style="text-align: center">Your Contacts</th>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Age</th>
                                <th>Email</th>
                                <th>ZipCode</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($matches) && count($matches)>0)
                                @foreach($matches as $match)
                                    <tr>
                                        <td>{{$match->first_name}} {{$match->last_name}}</td>
                                        <td>{{($match->gender == 'M')?'Male':'Female'}}</td>
                                        <td>{{$match->age}}</td>
                                        <td>{{$match->email}}</td>
                                        <td>{{$match->zip_code}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @else
                            <table class="table-primary table-bordered table-hover" align="center" width="100%" cellpadding="10">
                                <thead>
                                <tr>
                                    <th colspan="5" style="text-align: center">Your Agent</th>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Age</th>
                                    <th>Profession</th>
                                    <th>Email</th>
                                    <th>ZipCode</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$agent->first_name}} {{$agent->last_name}}</td>
                                    <td>{{($agent->gender == 'M')?'Male':'Female'}}</td>
                                    <td>{{$agent->age}}</td>
                                    <td>{{$agent->profession}}</td>
                                    <td>{{$agent->email}}</td>
                                    <td>{{$agent->zip_code}}</td>
                                </tr>
                                </tbody>
                            </table>
                            <p><b>Feel free to contact him!</b></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
