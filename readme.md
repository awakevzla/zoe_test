#Zoe Financial: Engineering Test

Congratulations! We’re glad to have you here in this stage of the process. We need to test your
code expertise and your ability to come up with a solution. We’re going to evaluate your solution
using the following criteria:

- Solution actually works.
- Code is easy to read.
- Code is testable.
- Solution is well documented.

This document explains the step-by-step specification for developing a solution. We’ve detailed
what are the resources you can use to build your solution but you can use any extra resource
you consider properly to achieve the results.

##Specification
Please first read this document to understand how the evaluation-solution process works. You need to build a solution using the following:

- PHP + Laravel Framework.
- MVC (Model View Controller)
- MySQL/PostgreSQL for databases.
- Javascript.
- Unit tests for your methods.
- Provide a README file in your solution with the process to replicate the environment,
  listing the 3rd party libraries or resources needed to run it and a brief description about
  how it works.
  
The system has 2 lists of users: Agents and Contacts. Agents are the users who want to
connect with Contacts for offering professional services. Contacts are users who are being
reached by Agents to solve specific problems. Each user type has (first name, last name, age,
gender, zip code) but only Agents have 1 extra field (profession). Professions are (Corporate
Finance, Commercial Banking, Investment Banking, Hedge Funds, Financial Planning).

##Problem

How can the system match each Agent with ​ n ​ Contacts based on distance? E.g.: Agent John
Smith (San Francisco) got a match with Contact Jane Doe (San Francisco), and Contact John
Doe (Los Angeles).


##GUI

- A view for User Login.
- A view for User Details.
- A view for Listing the matches they got.

#Deployment

##System Requirements

- Php 7.X (openssl,mbstring,bcmath,mysql-pgsql,zip,xml,gd,sqlite3)
- Apache2 or NGINX
- Sqlite3 (To run the tests)
- Mysql/Postgresql to store application data
- Composer

##Installation

- Copy the .env.example file on .env
- Fill the following properties:
    - DB_CONNECTION= `mysql - pgsql`
    - DB_HOST= `localhost`
    - DB_PORT= `3306 - 5432`
    - DB_DATABASE= `database name`
    - DB_USERNAME= `database username with privileges`
    - DB_PASSWORD= `database user password`
    - GOOGLE_KEY= `Google key`
    - CRON_EXPRESSION= `Regularity with which the procedure is executed to find matches`

CD into the directory of this project and run the following commands:

1. `composer install`
2. `php artisan key:generate`
3. `php artisan migrate --seed`
4. `php artisan users:match` Just the first time to initialize the data

Configure your local crontab to run matches procedure automatically:
- `crontab -e`
- Copy this on your crontab:
    ```
    * * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
    ```
- Close and safe the changes

##Run the service

- Run `php artisan serve` on project directory
- Open [http://localhost:8000](http://localhost:8000)
- Login into the platform and see your matches
    - Credentials to login:
        - **Email:** testing@zoe.io
        - **Password:** 123123
        
##Run tests
Run the following command to execute tests:

    
    php vendor/bin/phpunit
        
#3rd Party Library used

- Google Distance Matrix api to calculate distance between two zip codes.


#How it works

The platform make two groups of users (Agents and Contacts) and besides that
group them by his zip code; then the algorithm calculate the distance between
the group of Agents and Contacts and match it with the closest groups.

On the other hand, to improve the time of the recurrent matches; the platform
save the zip codes distance on database to avoid the unnecessary request on
the Google API and reduce costs.  