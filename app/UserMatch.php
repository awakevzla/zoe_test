<?php

namespace ZoeTest;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserMatch extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','agent_id','distance'];
}
