<?php

namespace ZoeTest\Http\Controllers;

use ZoeTest\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::find(Auth::id());
        Log::info("Logged User: ".$user);
        $matches = [];
        $agent = null;
        if($user->profession_id){
            $matches = DB::table('user_matches')
                ->join('users', 'users.id', '=', 'user_matches.user_id')
                ->where('user_matches.agent_id', '=', $user->id)
                ->select('users.*')
                ->get();
        }else{
            $agent = DB::table('user_matches')
                ->join('users', 'users.id', '=', 'user_matches.agent_id')
                ->join('professions', 'professions.id', '=', 'users.profession_id')
                ->where('user_matches.user_id', '=', $user->id)
                ->select('users.*', 'professions.name as profession')
                ->first();
        }
        return view('home')->with(['matches'=>$matches, 'agent'=>$agent]);
    }

    public function profile()
    {
        $user = User::find(Auth::id());
        return view('profile')->with(['user'=>$user]);
    }
}
