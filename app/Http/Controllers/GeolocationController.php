<?php

namespace ZoeTest\Http\Controllers;

use ZoeTest\Console\Commands\UserMatches;
use ZoeTest\User;
use ZoeTest\UserMatch;
use ZoeTest\ZipDistance;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class GeolocationController extends Controller
{
    /**
     * Function to get distance between two zip codes, could be retrieve from database or google maps api.
     *
     * @param $zipCodeOrigin
     * @param $zipCodeDestination
     * @return false|int
     */
    public function getDistance($zipCodeOrigin, $zipCodeDestination)
    {
        $zipDistance = ZipDistance::where(function ($q) use ($zipCodeOrigin, $zipCodeDestination){
            $q->where('origin', '=', $zipCodeOrigin)->where('destination', '=', $zipCodeDestination);
        })->orWhere(function ($q) use ($zipCodeOrigin, $zipCodeDestination){
            $q->where('origin', '=', $zipCodeDestination)->where('destination', '=', $zipCodeOrigin);
        })->first();

        if($zipDistance){

            return $zipDistance->distance;
        }

        return $this->calculateDistance($zipCodeOrigin,$zipCodeDestination);
    }

    /**
     * Function to calculate distance using GoogleMaps
     *
     * @param $zipCodeOrigin
     * @param $zipCodeDestination
     * @return int|false Indicates the distance in meters between two zip codes
     */

    public function calculateDistance($zipCodeOrigin, $zipCodeDestination)
    {
        $googleKey = env('GOOGLE_KEY');
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$zipCodeOrigin&destinations=$zipCodeDestination&key=$googleKey";

        $client = new Client();

        $res = $client->get($url);
        $status = $res->getStatusCode();
        $stream = $res->getBody();
        $contents = $stream->getContents();

        Log::info("Response from Maps Request for distance between ".$zipCodeOrigin." and ".$zipCodeDestination." - StatusCode: ".$res->getStatusCode()." Body: ".$contents);

        $contents = json_decode($contents);

        Log::info("Contents: ".$contents->status);
        if($status == 200){
            if($contents->status != 'OK'){
                return false;
            }

            $response = isset($contents->rows[0]->elements[0]->distance->value)?$contents->rows[0]->elements[0]->distance->value:false;

            if($response)
                ZipDistance::create(['origin'=>$zipCodeOrigin,'destination'=>$zipCodeDestination,'distance'=>$response]);

            return $response;
        }

        return false;
    }

    /**
     * Main function to create matches between group of Agents wand group of Contacts
     */
    public function createMatches()
    {
        $agents = User::whereNotNull('profession_id')->get();
        $contacts = User::whereNull('profession_id')
            ->leftJoin("user_matches", "user_matches.user_id", "=", "users.id")
            ->whereNull("user_matches.id")
            ->select("users.*")
            ->get();
        $agentsGrouped = [];
        $contactsGrouped = [];

        foreach ($agents as $agent){
            if(!isset($agentsGrouped[$agent->zip_code]))
                $agentsGrouped[$agent->zip_code] = [];
            array_push($agentsGrouped[$agent->zip_code], $agent);
        }

        foreach ($contacts as $contact){
            if(!isset($contactsGrouped[$contact->zip_code]))
                $contactsGrouped[$contact->zip_code] = [];
            array_push($contactsGrouped[$contact->zip_code], $contact);
        }
        if(count($contactsGrouped) <= 0){
            Log::info("We have no contacts to match");
            return;
        }

        if(count($agentsGrouped) <= 0){
            Log::info("We hae no agents to match");
            return;
        }

        Log::info("Agents Grouped: ".json_encode($agentsGrouped));
        Log::info("Contacts Grouped: ".json_encode($contactsGrouped));

        foreach ($contactsGrouped as $k=>$contactsGroup){
            if(isset($agentsGrouped[$k])){
                $agentsGroup = $agentsGrouped[$k];
                Log::info("I found the same zip on contacts".json_encode($contactsGroup)." to agents: ".json_encode($agentsGroup));

                $this->match($agentsGroup,$contactsGroup, 0);
            }else{
                $closestZipCode = null;
                $closestDistance = null;
                foreach ($agentsGrouped as $zc=>$agentsGroup){

                    $distance = $this->getDistance($k, $zc);
                    if(!$closestZipCode){
                        $closestZipCode = $zc;
                        $closestDistance = $distance;
                    }
                    else{
                        if($closestDistance > $distance){
                            $closestDistance = $distance;
                            $closestZipCode = $zc;
                        }
                    }
                }

                if($closestZipCode and $closestDistance){
                    $agentsGroup = $agentsGrouped[$closestZipCode];
                    $this->match($agentsGroup,$contactsGroup,$closestDistance);
                }
            }
        }
    }

    /**
     * Function to create user matches rows establishing relation between Agent and Contact
     *
     * @param $agentsGroup
     * @param $contactsGroup
     * @param $closestDistance
     */

    public function match($agentsGroup, $contactsGroup, $closestDistance)
    {
        $position = 0;
        foreach ($contactsGroup as $contact){
            if($position >= count($agentsGroup))
                $position = 0;
            UserMatch::create(['user_id'=>$contact->id,'agent_id'=>$agentsGroup[$position]->id,'distance'=>$closestDistance]);
            Log::info("I'm gonna match ".$contact->first_name." - ".$contact->zip_code." with ".$agentsGroup[$position]->first_name." - ".$agentsGroup[$position]->zip_code);
            $position++;
        }
    }
}
