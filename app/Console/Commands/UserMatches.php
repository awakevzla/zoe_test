<?php

namespace ZoeTest\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UserMatches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:match';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to match Agents with Contacts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Starting Procedure to match Agents with Contacts");
        Log::info("Starting Procedure to match Agents with Contacts");
        $controller = app()->make('ZoeTest\Http\Controllers\GeolocationController');
        $response = app()->call([$controller, 'createMatches'], []);

        Log::info("End of the procedure to match Agents with Contacts");
        $this->info("End of the procedure to match Agents with Contacts");
    }
}
