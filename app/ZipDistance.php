<?php

namespace ZoeTest;

use Illuminate\Database\Eloquent\Model;

class ZipDistance extends Model
{
    protected $fillable = ['origin', 'destination','distance'];
}
