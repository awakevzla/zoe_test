<?php

namespace ZoeTest;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profession extends Model
{
    //protected $table = 'professions';
    use SoftDeletes;
    protected $fillable = ['name'];

    public function users()
    {
        return $this->hasMany('ZoeTest\User');
    }
}
